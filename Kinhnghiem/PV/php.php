<?php
1. neu thuoc tinh la private ma duoc truy xuat tu ngoai lop truc tiep thi k the truy xuat duoc
2. neu thuoc tinh la private ma duoc truy xuat tu ngoai lop ma thong qua mot method co' muc do truy cap la public thi truy xuat duoc, nhung qua mot method co muc do truy cap la protected thi khong the truy xuat duoc, va method co muc do truy cap la private thi cang khong the duoc.
3.neu goi mot thuoc tinh co muc do truy xuat la private truc tiep tu mot class ke thua thi se k truy xuat duoc.
4.neu goi mot thuoc tinh co muc do truy xuat la private tu mot class ket thua thong qua method co muc do truy cap la "public" ,"protected","private" neu method do' la method cua class ke thua thi se khong the truy cap duoc. Con neu method do la method cua class parent thi pham vi truy cap cua method do' la public va protected se truy cap duoc con private se khong truy cap duoc

5. cac tinh chat dac thu cua huong doi tuong
	- tinh ke thua
	- tinh dong goi 
	- tinh da hinh
	- tinh truu tuong
	6. dac diem lop truu tuong
	- Các phương thức ( hàm ) khi được khai báo là abstract thì chỉ được định nghĩa chứ không được phép viết code xử lý trong phương thức.
	- Trong abstract class nếu không phải là phương thức abstract thì vẫn khai báo và viết code được như bình thường.
	- Phương thức abstract chỉ có thể khai báo trong abstract class. 
	- Các thuộc tính trong abstract class thì không được khai báo là abstract.
	- Không thể khởi tạo một abstract class.
	- Vì không thể khởi tạo được abstract class nên các phương thức chỉ được khai báo ở mức độ protected và public.
	- Các lớp kế thừa một abstract class phải định nghĩa lại tất cả các phương thức trong abstract class đó ( nếu không sẽ bị lỗi).
	7. tinh chat cua interface

	- interface khong phai la doi tuong
	- interface khong the khoi tao doi tuong
	- trong interface chung ta chi duoc khai bao phuong thuc chu khong duoc dinh nghia chung
	- trong interface chung ta chi khai bao duoc hang chu khong khai bao duoc bien
	- cac  lop implement interface phai khai bao va dinh nghia lai cac phuong thuc trong interface
	- mot class co the implement nhieu interface
	- moi interface co the ke thua lan nhau

	8. cac loai magic method
	- __construct()
	- __destruct()
	- __set()
	- __get()
	- __call()
	- __callstatic()
	- __sleep()
	- __wakeup()
	- __isset()
	- __unset()
	- con vai magic nua

	9. tinh chat final class

	- la class cuoi cung nen khong mot class nao co the ke thua
	- khi khai bao mot phuong thuc la final thi khong mot phuong thuc nao co the override lai phuong thuc do
	- phuong thuc final thi su dung trong class nao cung duoc