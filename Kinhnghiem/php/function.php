<?php

function checkDuplicateLocation($post_id,$taxonomy_id){
    global $wpdb;
    $query_location = "SELECT wtr.object_id
    FROM wp_term_relationships wtr
    WHERE wtr.object_id =".$post_id ." and wtr.term_taxonomy_id = ".$taxonomy_id; 
   $check_dubplicate_exists = $wpdb->get_results($query_location, OBJECT);
   if(!$check_dubplicate_exists){
      $wpdb->insert('wp_term_relationships', array('object_id'=>$post_id, 'term_taxonomy_id'=>$taxonomy_id));
    }
}
function getLocationNoExists($post_id,$key_location,$locations){
    global $wpdb;
    $query_location = "SELECT wt.name,wt.term_id,wtt.term_taxonomy_id
    FROM wp_terms wt
    INNER JOIN wp_term_taxonomy wtt ON wt.term_id = wtt.term_id
    WHERE wtt.taxonomy ='".$key_location."'"; 
   $check_location_exists = $wpdb->get_results($query_location, OBJECT);
   if($check_location_exists){
        foreach($check_location_exists as $val){
            $location_name = strtolower($val->name);
            if(strtolower($locations) == $location_name){
                $taxonomy_id = $val->term_taxonomy_id;
            }
        }

        if(isset($taxonomy_id )){
            checkDuplicateLocation($post_id,$taxonomy_id);
        }
    }
}
function getLocation($post_id,$key_location,$locations){
    global $wpdb;
    $query_location = "SELECT wt.name,wt.term_id,wtt.term_taxonomy_id
    FROM wp_terms wt
    INNER JOIN wp_term_taxonomy wtt ON wt.term_id = wtt.term_id
    WHERE wtt.taxonomy ='".$key_location."'"; 
    
   $check_location_exists = $wpdb->get_results($query_location, OBJECT);
   if($check_location_exists){
        foreach($check_location_exists as $val){
            $location_name = strtolower($val->name);
            if(strtolower($locations) == $location_name){
                $taxonomy_id = $val->term_taxonomy_id;
            }
        }

        if(isset($taxonomy_id )){
            checkDuplicateLocation($post_id,$taxonomy_id);
        }else{

             $wpdb->insert('wp_terms', array('name'=>$locations,'slug'=>str_replace(' ','-',strtolower($locations))));
            $query_insert_location = "SELECT MAX(wt.term_id) as max_term_id FROM wp_terms wt"; 
             $check_insert_location_exists = $wpdb->get_results($query_insert_location, OBJECT);
            if($check_insert_location_exists){
                foreach($check_insert_location_exists as $val){
                    $term_id = $val->max_term_id;
                }
                if(isset($term_id)){
                    $wpdb->insert('wp_term_taxonomy', array('term_id'=>$term_id,'taxonomy'=>'property_city'));
                    getLocationNoExists($post_id,$key_location,$locations);
                }
                
            }
        }  
   }
   
}

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();

    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}



function Generate_Thumbnail_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();

    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
}

function create_user_auto($username,$email,$password){
    $user_id = username_exists( $username );
    if ( !$user_id && email_exists($email) == false ) {
        $user_id = wp_create_user( $username, $password, $email );
        if( !is_wp_error($user_id) ) {
            $user = get_user_by( 'id', $user_id );
            $user->set_role( 'author' );
        }
    }
}

    $upload_dir = wp_upload_dir();
    $data_file_dir = $upload_dir['basedir'].'/xml/property1.xml';;
    $uploads_path = $upload_dir['baseurl'];
    $mage_path = $uploads_path.'/images/';
    $data_path = $uploads_path.'/xml/property1.xml';
    $data_attachment = $uploads_path.'/xml/pdf/document.pdf';
    $table_name  = $wpdb->prefix."posts";
    $post_status = 'draff';
    $conditon_agent = 'estate_agent';
    $conditon_property = 'estate_property';
    $user_id =1;

    
    if (file_exists($data_file_dir)) {
        //update post_status ='draff' if all post_type is estate_property  
        $wpdb->query( $wpdb->prepare("UPDATE $table_name 
                SET post_status = %s 
             WHERE post_type = %s",$post_status, $conditon_property)
        );
        $data_xml=simplexml_load_file($data_path) or die("Error: không thể tạo đối tượng"); //get data from file xml
        foreach($data_xml as $key=>$items){
                $refno_prop = (string)$items->refno_prop;
                $adv_text   = (string)$items->adv_text;
                $prop_name  = (string)$items->prop_name;
                $bullet1    = (string)$items->bullet1;
                $bullet2    = (string)$items->bullet2;
                $bullet3    = (string)$items->bullet3;
                $bullet4    = (string)$items->bullet4;
                $bullet5    = (string)$items->bullet5;
                $bullet6    = (string)$items->bullet6;
                $bullet7    = (string)$items->bullet7;
                $bullet8    = (string)$items->bullet8;
                $region     = (string)$items->region;
                $district   = (string)$items->district;
                $city       = (string)$items->city;
                $county       = (string)$items->county;
                $pcode       = (string)$items->pcode;
                $beds       = (string)$items->beds;
                $recs       = (string)$items->recs;
                $baths       = (string)$items->baths;
                $prop_type       = (string)$items->prop_type;
                $let_type       = (string)$items->let_type;
                $rentpwk       = (string)$items->rentpwk;
                $rentpcm       = (string)$items->rentpcm;
                $deposit       = (string)$items->deposit;
                $adv_period       = (string)$items->adv_period;
                $adv_rent       = (string)$items->adv_rent;
                $adv_sales       = (string)$items->adv_sales;
                $forsale       = (string)$items->forsale;
                $furnished       = (string)$items->furnished;
                $garden       = (string)$items->garden;
                $commsize       = (string)$items->commsize;
                $energy_graph       = (string)$items->energy_graph;
                $enviro_graph       = (string)$items->enviro_graph;

                $prop_ref       = (string)$items->prop_ref;
                $option_fee       = (string)$items->option_fee;
                $brochure       = (string)$items->brochure;
                $let_agreed       = (string)$items->let_agreed;
                $date_added       = (string)$items->date_added;
                $feature1       = strtolower(str_replace(' ','_', trim((string)$items->feature1)));
                $feature2       = strtolower(str_replace(' ','_', trim((string)$items->feature2)));
                $feature3       = strtolower(str_replace(' ','_', trim((string)$items->feature3)));
                $prop_status       = (string)$items->prop_status;
                $prop_summary       = (string)$items->prop_summary;
                $image1 = (string)$items->image1;
                $image2 = (string)$items->image2;
                $image3 = (string)$items->image3;
                $image4 = (string)$items->image4;
                $image5 = (string)$items->image5;
                $image6 = (string)$items->image6;
                $image7 = (string)$items->image7;
                $image8 = (string)$items->image8;

                $description = '';
                $description = $adv_text.'<br/>'.$bullet1.'<br/>'.$bullet2.'<br/>'.$bullet3.'<br/>'.$bullet4.'<br/>'.$bullet5.'<br/>'.$bullet6.'<br/>'.$bullet7.'<br/>'.$bullet8;
                $querystr = "SELECT $wpdb->posts.* 
                FROM $wpdb->posts
                WHERE $wpdb->posts.refno_prop ='".$refno_prop."' AND $wpdb->posts.post_type = 'estate_property'"; 
                $check_post_exists = $wpdb->get_results($querystr, OBJECT);
                foreach($check_post_exists as $value){
                     $getPostId = (int)$value->ID;
                }
                if(!$check_post_exists){
                    $defaults = array(
                        'post_author' => $user_id,
                        'post_content' => $description,
                        'post_content_filtered' => '',
                        'post_title' => $prop_name,
                        'post_excerpt' => '',
                        'post_status' => 'publish',
                        'post_type' => 'estate_property',
                        'comment_status' => 'open',
                        'ping_status' => 'open',
                        'post_password' => '',
                        'to_ping' =>  '',
                        'pinged' => '',
                        'post_parent' => 0,
                        'menu_order' => 0,
                        'guid' => '',
                        'import_id' => 0,
                        'context' => '',        
                    );
                    $post_id =  wp_insert_post($defaults); // insert row to table wp_posts
                    $wpdb->update('wp_posts', array('refno_prop' =>$refno_prop), array( 'ID' => $post_id ) );
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_address','meta_value'=>$district));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_zip','meta_value'=>$pcode));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_bedrooms','meta_value'=>$beds));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-recs','meta_value'=>$recs));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_bathrooms','meta_value'=>$baths));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-type','meta_value'=>$prop_type));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-let-type','meta_value'=>$let_type));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-rentpwk','meta_value'=>$rentpwk));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_price','meta_value'=>$rentpcm));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-deposit','meta_value'=>$deposit));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-rent-period','meta_value'=>$adv_period));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-adv-rent','meta_value'=>$adv_rent));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-adv-sales','meta_value'=>$adv_sales));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-forsale-price','meta_value'=>$forsale));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-furnished','meta_value'=>$furnished));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-garden','meta_value'=>$garden));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_size','meta_value'=>$commsize));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-energy-graph','meta_value'=>$energy_graph));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-enviro-graph','meta_value'=>$enviro_graph));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-reference','meta_value'=>$prop_ref));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-option-fee','meta_value'=>$option_fee));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-brochure-url','meta_value'=>$brochure));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-let-agreed','meta_value'=>$let_agreed));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-date','meta_value'=>$date_added));
                   
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_status','meta_value'=>$prop_status));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property-summary','meta_value'=>$prop_summary));


                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_google_view','meta_value'=>'1'));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_longitude','meta_value'=>'-74.1346181'));
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'property_latitude','meta_value'=>'40.5721819'));
                    if(!empty($feature1)){
                        $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>$feature1,'meta_value'=>'1'));
                    }
                    if(!empty($feature2)){
                        $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>$feature2,'meta_value'=>'1'));
                    }
                    if(!empty($feature3)){
                        $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>$feature3,'meta_value'=>'1'));
                    }

                    $fileImages = $upload_dir['basedir'] . '/images/'.$image1;
                    if(!empty($image1)){
                        Generate_Featured_Image($fileImages ,$post_id);
                    }
                    $fileImage2 = $upload_dir['basedir'] . '/images/'.$image2;
                    if(!empty($image2)){
                        Generate_Thumbnail_Image($fileImage2 ,$post_id);
                    }
                    $fileImage3 = $upload_dir['basedir'] . '/images/'.$image3;
                    if(!empty($image3)){
                        Generate_Thumbnail_Image($fileImage3,$post_id);
                    }
                    $fileImage4 = $upload_dir['basedir'] . '/images/'.$image4;
                    if(!empty($image4)){
                        Generate_Thumbnail_Image($fileImage4,$post_id);
                    }
                    $fileImage5 = $upload_dir['basedir'] . '/images/'.$image5;
                    if(!empty($image5)){
                        Generate_Thumbnail_Image($fileImage5,$post_id);
                    }

                    $fileImage6 = $upload_dir['basedir'] . '/images/'.$image6;
                    if(!empty($image6)){
                        Generate_Thumbnail_Image($fileImage6,$post_id);
                    }

                    $fileImage7 = $upload_dir['basedir'] . '/images/'.$image7;
                    if(!empty($image7)){
                        Generate_Thumbnail_Image($fileImage7,$post_id);
                    }

                    $fileImage8 = $upload_dir['basedir'] . '/images/'.$image8;
                    if(!empty($image8)){
                        Generate_Thumbnail_Image($fileImage8,$post_id);
                    }

                    getLocation($post_id,"property_city",$city);
                    getLocation($post_id,"property_area",$region);
                    getLocation($post_id,"property_county_state",$county);
                    
                }else{
                    if(isset($getPostId)){
                        $wpdb->update('wp_posts', array('post_content' => $description,'post_title' => $prop_name, 'post_status' => 'publish'), array('ID' => $getPostId ) ); 
                        update_post_meta($getPostId,'property_address',$district);
                        update_post_meta($getPostId,'property_zip',$pcode);
                        update_post_meta($getPostId,'property_bedrooms',$beds);
                        update_post_meta($getPostId,'property-recs',$recs);
                        update_post_meta($getPostId,'property_bathrooms',$baths);
                        update_post_meta($getPostId,'property-type',$prop_type);
                        update_post_meta($getPostId,'property-let-type',$let_type);
                        update_post_meta($getPostId,'property-rentpwk',$rentpwk);
                        update_post_meta($getPostId,'property_price',$rentpcm);
                        update_post_meta($getPostId,'property-deposit',$deposit);
                        update_post_meta($getPostId,'property-rent-period',$adv_period);
                        update_post_meta($getPostId,'property-adv-rent',$adv_rent);
                        update_post_meta($getPostId,'property-adv-sales',$adv_sales);
                        update_post_meta($getPostId,'property-forsale-price',$forsale);
                        update_post_meta($getPostId,'property-furnished',$furnished);
                        update_post_meta($getPostId,'property-garden',$garden);
                        update_post_meta($getPostId,'property_size',$commsize);
                        update_post_meta($getPostId,'property-energy-graph',$energy_graph);
                        update_post_meta($getPostId,'property_enviro_graph',$enviro_graph);
                        update_post_meta($getPostId,'property-reference',$prop_ref);
                        update_post_meta($getPostId,'property-option-fee',$option_fee);
                        update_post_meta($getPostId,'property-brochure-url',$brochure);
                        update_post_meta($getPostId,'property-let-agreed',$let_agreed);
                        update_post_meta($getPostId,'property-date',$date_added);
                        if(!empty($feature1)){
                            update_post_meta($getPostId,$feature1,'1');
                        }
                        if(!empty($feature2)){
                            update_post_meta($getPostId,$feature2,'1');
                        }
                        if(!empty($feature3)){
                            update_post_meta($getPostId,$feature3,'1');
                        }
                        update_post_meta($getPostId,'property_status',$prop_status);
                        update_post_meta($getPostId,'property-summary',$prop_summary);
                    }
                    
                }

        }   
    }else {
        exit('Failed to open property.xml.');
    }
        
    $data_file_dir_tenant = $upload_dir['basedir'].'/xml/tenant_header1.xml';;
    $uploads_path_tenant = $upload_dir['baseurl'];
    $data_path_tenant = $uploads_path_tenant.'/xml/tenant_header1.xml';
    if (file_exists($data_file_dir_tenant)) {
        ////update post_status ='draff' if all post_type is estate_agent  
       $wpdb->query( $wpdb->prepare("UPDATE $table_name 
                SET post_status = %s 
             WHERE post_type = %s",$post_status, $conditon_agent)
        );
        $data_tenant_xml=simplexml_load_file($data_path_tenant) or die("Error: không thể tạo đối tượng"); //get data from file xml
        foreach($data_tenant_xml as $key=>$items){
                $querystr = "SELECT $wpdb->posts.* 
                FROM $wpdb->posts
                WHERE $wpdb->posts.refno_prop ='".$items->refno_ten."' AND $wpdb->posts.post_type = 'estate_agent'"; 
                $check_post_exists = $wpdb->get_results($querystr, OBJECT);
                foreach($check_post_exists as $value){
                     $getPostId = (int)$value->ID;
                }
                $agent_custom_data =array();
                if(!empty($items->term)){
                    $agent_custom_data[0]['label']="Tenant’s letting address";
                    $agent_custom_data[0]['value']=$prop_name;   
                }
                if(!empty($items->term)){
                    $agent_custom_data[1]['label']="Term of tenancy";
                    $agent_custom_data[1]['value']=(string)$items->term;    
                }
                if(!empty($items->frequency)){
                    $agent_custom_data[2]['label']="Rent frequency";
                    $agent_custom_data[2]['value']=(string)$items->frequency;   
                }
                if(!empty($items->rent)){
                    $agent_custom_data[3]['label']="Rent charged";
                    $agent_custom_data[3]['value']=(string)$items->rent;    
                }
                if(!empty($items->deposit)){
                    $agent_custom_data[4]['label']="Deposit";
                    $agent_custom_data[4]['value']=(string)$items->deposit; 
                }
                if(!empty($items->balance)){
                    $agent_custom_data[5]['label']="Tenant’s balance";
                    $agent_custom_data[5]['value']=(string)$items->balance; 
                }
                if(!empty($items->pcode)){
                    $agent_custom_data[6]['label']="Post Code";
                    $agent_custom_data[6]['value']=(string)$items->pcode;   
                }
                if(!empty($items->ten_start)){
                    $agent_custom_data[7]['label']="Start date of Tenancy";
                    $agent_custom_data[7]['value']=(string)$items->ten_start;   
                }
                if(!empty($items->ten_end)){
                    $agent_custom_data[8]['label']="End date of Tenancy";
                    $agent_custom_data[8]['value']=(string)$items->ten_end; 
                }

                $agent_custom_data =array_values($agent_custom_data);
                $get_agent_data = serialize($agent_custom_data);
                //create_user_auto($items->tenant,$items->login,$items->password);
                if(!$check_post_exists){
                    $defaults = array(
                        'post_author' => $user_id,
                        'post_content' =>'',
                        'post_content_filtered' => '',
                        'post_title' => $items->tenant,
                        'post_excerpt' => '',
                        'post_status' => 'publish',
                        'post_type' => 'estate_agent',
                        'comment_status' => 'open',
                        'ping_status' => 'open',
                        'post_password' => '',
                        'to_ping' =>  '',
                        'pinged' => '',
                        'post_parent' => 0,
                        'menu_order' => 0,
                        'guid' => '',
                        'import_id' => 0,
                        'context' => '',        
                    );

                    $post_id =  wp_insert_post($defaults); // insert row to table wp_posts
                    $wpdb->update('wp_posts', array('refno_prop' =>$items->refno_ten), array( 'ID' => $post_id ) );
                    $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'agent_email','meta_value'=>(string)$items->login));
                    if(is_serialized($get_agent_data)){
                        $wpdb->insert('wp_postmeta', array('post_id'=>$post_id, 'meta_key'=>'agent_custom_data','meta_value'=>$get_agent_data));
                    }   
                }else{
                    if(isset($getPostId)){
                        $wpdb->update('wp_posts', array('post_title' => $items->tenant, 'post_status' => 'publish'), array('ID' => $getPostId ) ); 
                        update_post_meta($getPostId,'agent_email',(string)$items->login);
                        if(is_serialized($get_agent_data)){
                            update_post_meta($getPostId,'agent_custom_data',(string)$get_agent_data);
                        }
                        
                    }
                }
        }

    }else {
        exit('Failed to open tenant_header.xml.');
    }
?>