doc tai lieu o day 

https://www.elastic.co/guide/en/elasticsearch/reference/5.5/deb.html#deb-running-systemd

https://github.com/Smile-SA/elasticsuite/wiki/ModuleInstall

1. cai dat service elastic search

	sudo wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.5.3.deb
	sudo sha1sum elasticsearch-5.5.3.deb 
	sudo dpkg -i elasticsearch-5.5.3.deb
- the la da cai xong: chay lenh 
	sudo -i service elasticsearch stop   //// tat service elasticsearch
	sudo -i service elasticsearch start  // bat service elasticsearch
	

- kiem tra xem da chay chua ? 
	+ curl localhost:9200
- go~ cd /usr/share/elasticsearch
	Install plugins :
		bin/elasticsearch-plugin install analysis-phonetic
		bin/elasticsearch-plugin install analysis-icu
		sudo service elasticsearch restart
2. cai dat module tich hop elasticsearch

- chay lenh:
	composer require smile/elasticsuite

- chay lenh:
	bin/magento module:enable Smile_ElasticsuiteCore Smile_ElasticsuiteCatalog Smile_ElasticsuiteSwatches Smile_ElasticsuiteCatalogRule Smile_ElasticsuiteVirtualCategory Smile_ElasticsuiteThesaurus Smile_ElasticsuiteCatalogOptimizer Smile_ElasticsuiteTracker

	bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/servers localhost:9200,localhost:9200
	bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/enable_https_mode 0
	bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/enable_http_auth 0
	bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/http_auth_user ""
	bin/magento config:set -l smile_elasticsuite_core_base_settings/es_client/http_auth_pwd ""
	bin/magento app:config:import


	bin/magento setup:upgrade 


	bin/magento index:reindex catalogsearch_fulltext
	bin/magento index:reindex elasticsuite_categories_fulltext
	bin/magento index:reindex elasticsuite_thesaurus