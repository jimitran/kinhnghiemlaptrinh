1.De bai 
	#513 Disable and redirect some Eternity band categories

	Please disable and do 301 redirect of these 3 categories:
	https://www.itshot.com/jewelry/diamond-eternity-bands/14k-gold
	https://www.itshot.com/jewelry/diamond-eternity-bands/18k-gold
	https://www.itshot.com/jewelry/diamond-eternity-bands/platinum

	to their parent: https://www.itshot.com/jewelry/diamond-eternity-bands

	Also please don't forget to assign all products from those 3 categories to https://www.itshot.com/jewelry/diamond-eternity-bands
 
 Loi giai:
	
	Step 1: Run sql to move products from old category to new category.
	Change to new category product:
	
	UPDATE `tsht_catalog_category_product` AS `a`, (SELECT * FROM `tsht_catalog_category_product` WHERE `category_id` IN (13457, 13458, 13459) AND `product_id` not in (select `product_id` from `tsht_catalog_category_product` where `category_id` = 13377)) as `b` SET `a`.`category_id` = 13377 WHERE `a`.`category_id` = `b`.`category_id` AND `a`.`product_id` = `b`.`product_id`


	Step 2: Run sql to delete duplicate rows in category table.
	
	Delete duplicate category product:
	DELETE FROM `tsht_catalog_category_product` WHERE `category_id` IN (13457, 13458, 13459)

	Step 3: Go to Admin -> Catalog -> Disable the categories: 13457, 13458, 13459
	
	Step 4: Run re-index at: System-> Index Management-> select Category Product.
	