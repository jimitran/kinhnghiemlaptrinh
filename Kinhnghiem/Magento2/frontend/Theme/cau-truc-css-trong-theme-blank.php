
magento_theme_blank co 2 file style chinh: styles-m.less va styles-l.less

styles-m.less
    'source/_reset.less';
    '_styles.less';
        'source/lib/_lib.less';
                @import '_actions-toolbar.less';
                @import '_breadcrumbs.less';
                @import '_buttons.less';
                @import '_dropdowns.less';
                @import '_forms.less';
                @import '_grids.less';
                @import '_icons.less';
                @import '_layout.less';
                @import '_loaders.less';
                @import '_messages.less';
                @import '_navigation.less';
                @import '_pages.less';
                @import '_popups.less';
                @import '_rating.less';
                @import '_resets.less';
                @import '_sections.less';
                @import '_tables.less';
                @import '_tooltips.less';
                @import '_typography.less';
                @import '_utilities.less';
                @import '_variables.less';

                        @import 'variables/_colors.less';
                        @import 'variables/_typography.less';
                        @import 'variables/_tables.less';
                        @import 'variables/_layout.less';
                        @import 'variables/_responsive.less';
                        @import 'variables/_navigation.less';
                        @import 'variables/_sections.less';
                        @import 'variables/_buttons.less';
                        @import 'variables/_icons.less';
                        @import 'variables/_messages.less';
                        @import 'variables/_tooltips.less';
                        @import 'variables/_loaders.less';
                        @import 'variables/_forms.less';
                        @import 'variables/_pages.less';
                        @import 'variables/_rating.less';
                        @import 'variables/_dropdowns.less';
                        @import 'variables/_actions-toolbar.less';
                        @import 'variables/_breadcrumbs.less';
                        @import 'variables/_popups.less';
                        @import 'variables/_structure.less';
                        @import 'variables/_components.less';

        'source/_sources.less';
                @import '_variables.less';
                @import (reference) '_extends.less';
                @import '_typography.less';
                @import '_layout.less';
                @import '_tables.less';
                @import '_messages.less';
                @import '_navigation.less';
                @import '_tooltips.less';
                @import '_loaders.less';
                @import '_forms.less';
                @import '_icons.less';
                @import '_buttons.less';
                @import '_sections.less';
                @import '_pages.less'; // Theme pager
                @import '_actions-toolbar.less';
                @import '_breadcrumbs.less';
                @import '_popups.less';
                @import '_price.less';
        'source/_components.less';
                @import 'components/_modals.less'; // From lib
                @import 'components/_modals_extend.less'; // Local
    'source/_extends.less';
    'source/lib/_responsive.less';
    'source/_theme.less';

styles-l.less
    '_styles.less';
    'source/_extends.less';
    'source/lib/_responsive.less';
    'source/_theme.less';