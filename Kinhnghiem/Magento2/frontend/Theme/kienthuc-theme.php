1. Muon khai bao hoac thay doi gia tri cua variable cua theme phai thay doi trong __theme.less

2. Mot theme ma ke thua tu theme default magento thi nen extend hon la override styles. Bat cu khi nao co the can customize trong file __extend.less hoac __theme.less thay vi override mot file .less tu theme parent
3. Muon thay doi vi tri cua block hoac container thi can su dung node move
4. remove block hoac container su dung node remove hoac display attribute cua <referenceBlock>/<referenceContainer>
5. thay doi html tag hoac css class cho container da ton tai su dung <referenceContainer>

6. trong magento2 co 2 loai ui component : basic component and secondary component
	- basic component co': listing component va form component
	- tat ca ui component khac la secondary component

7. khi nao su dung ui component
	- PHTML template with inline JavaScript

	- PHTML template with declaration of related JavaScript file via XML layout

	- jQuery widget

	- Magento 2 UI component