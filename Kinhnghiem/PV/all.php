Sử dụng PMS (để quản lí các training request, các practice tasks)
SmartGIT và github (để quản lí các code practice và demo online)
PHPStorm (sử dụng làm tool chính đề edit code và search trong toàn bộ code khi cần)

Job training guide
- Sử dụng PMS (Whiteboard, My tasks)
- Sử dụng Basecamp
- Sử dụng FileZilla và SmartGIT
- Hệ thống test site va production, deploy via GIT command
- Move sites between local and servers
- Note lại các kiến thức và kinh nghiệm học được hàng ngày vào 1 file text
- Phải tự tay gõ code để nó thành kiến thức của mình, ko ngắm code, ko copy/paste.

Nguyên tắc vàng khi xử lí lỗi
- Mọi errors đều đi kèm 1 error message chi tiết (phụ thuộc vào setting - error có thể trả về và hiện thị trên browse hoặc log trong files)
- Errors được handle theo nhiều level application (sẽ theo qui định của application/open source), level PHP processor (những lỗi mang tính hệ thống, application sẽ ko thể catch dc và thường xuất hiện trong log của PHP), level Web Server (apache or nignx logs), OS log
- Ngoài các errors về mặt technical thì các errors về mặt logic sẽ cần sử dụng kĩ thuật debug, trace stack và data để tìm ra

A. Tools
- Avast Free Version (antivirus - có thể sử dụng 1 chương trình thay thế tương đương)
- Firefox, Chrome, Safari (http://www.techspot.com/downloads/4184-safari-for-windows.html), IE (browser)
- FileZilla (FTP/SFTP)
- Wamp (enable mod_rewrite, error log display, set higher memory_limit, execute_time, increase mysql_query_cache size)
- Skype (communication)
- SmartGIT (git client)
- PhpStorm (editor + fast search index), Notepad++ (editor)
- PerfectPixel Chrome extension (compare design)
- FireBug Firefox plugin (debugging javascript better)
- ColorPix (color selection - có thể sử dụng 1 chương trình thay thế tương đương)
- Lightshot (capture and upload screenshot online - có thể sử dụng 1 chương trình thay thế tương đương)
- Winmerge (compare files)
- Blisk (https://blisk.io devices simulators)
- php_enhanced_en.chm (handy manual for PHP) https://www.dropbox.com/s/dtp1hr9yiyh5j4x/php_enhanced_en.chm?dl=0
- Kitty (SSH access) https://www.dropbox.com/s/2jacd3qgpkvrn3t/kitty_portable.exe?dl=0
- TeamViewer (remote suppport)
- OpenVPN https://www.dropbox.com/s/dnffvw1pi3xzb74/openvpn-client.msi?dl=0
- Photoshop https://www.dropbox.com/sh/3417cdpeh6ttuch/AAAswKBT0PjCmDwThsTz2A1Ta?dl=0
- Illustrator https://www.dropbox.com/sh/dytuyjy6kdaekfr/AADIp_ikK7cz9TDHRXIjtWH4a?dl=0
- Office software, CSVed (for easier CSV edit)
- SageThumbs (quickly save PSD to JPG without photoshop)
- VirtualBox (và install 2 images https://bitnami.com/stack/lamp/virtual-machine https://bitnami.com/stack/nginx/virtual-machine )

===================================================================================================================================================

B. Basic web

1. PSD -> HTML 
- Ko responsive, ko sử dụng bootstrap
- Nắm được
+ CSS box model, position absolute, relative, static
+ Use before, after (Pseudo-elements), Pseudo-classes, attribute selector, combinators
+ Sprite
+ Custom web font-face
+ Fluid layout (100% width)
- biet su dung Inspect tool to view DOM, CSS,....)
- ghep menu dropdown, slideshow, lightrabox, grid,......
- practice and test on all browsers (FF, Chrome, IE, Safari) (8 -> 16 hours)
- CSS standard https://www.dropbox.com/s/lr3pc6rmzmensbl/css%20standard.txt?dl=0

2. HTML DOM -> Javascript DOM -> Javascript BOM (location, windows, document, screen, cookie) 
- Event (3 cách khác nhau để khai báo handler cho 1 event http://www.w3schools.com/jsref/event_onclick.asp) 
- Selector http://www.w3schools.com/jsref/met_document_queryselector.asp
- Biet su dung Console de print debug information and test nhanh cac variable, function call http://www.just4demo.biz/screenshots/2016-12-01_120958.png
- Biet su dung Debugger tab in Inspect tool de watch events,.... dat break point http://www.just4demo.biz/screenshots/2016-12-01_121353.png
- Practice some examples (back to top, sticky header on scroll) (8 -> 16 hours)
- Phân biệt được sự khác nhau của HTML DOM source được generate trước và sau JS execute
- Interval timer feature in Javascript

3. PHP Variable scope, Operators (comparision operator, logical operators) ,phân biệt dấu quote đơn '' và quote kép "", phân biệt is_null và isset, các tập hàm hay sử dụng
- Xử lí file, xử lí string, , xử lí date time, upload, kết nối mysql qua PDO, xử lí CURL
- Dùng qua 200 most popular functions http://www.boyter.org/2011/03/list-of-most-commonly-used-php-functions/

4. Responsive (bỏ HTML đã làm trong step 1 để làm lại theo tư duy responsive - sử dụng boostrap grid hoặc tự viết media query)
- https://developers.google.com/web/fundamentals/design-and-ui/responsive/
- https://developers.google.com/web/fundamentals/design-and-ui/responsive/patterns
- http://www.creativebloq.com/responsive-web-design/get-started-5132987
- Cần pratice để create 1 very good products grid for all screen size
- Luyện tư duy "mobile first" khi thiết kế CSS https://blog.donnywals.com/mobile-first-is-a-great-workflow/
- Xem về các grid responsive libraries and flexbox

5. HTML Form action/type -> HTTP GET, POST -> SESSION, COOKIE, $_SERVER trong PHP, HTTP Header
http://code.tutsplus.com/tutorials/http-headers-for-dummies--net-8039
http://www.thesolveproblems.com/view_computer_problems.php?solution=What%20is%20Difference%20Between%20Cookies%20and%20Sessions%20in%20php&ok=tTlvcFQUoOKpf9kwhnfzcgoaevkpmsFB79E2158CD6430&accsess=78CFD3AB54E1620
- Practice login (and remember login) 
- Biết các truyền data đi dạng array https://www.designedbyaturtle.co.uk/2014/submit-an-array-with-a-html-form/
- Biet su dung Network tab in Inspect tool de view HTTP Header information, monitor redirection,...... (6 -> 12 hours)

6. XMLHTTPRequest -> Ajax in Jquery -> JSON in JS -> JSON decode/encode in PHP
- Pactice "search as you type" demo (send Ajax search request -> respond HTML or JSON -> display result in a table) (4 -> 8 hours)
- Sử dụng Network tab in Inspect tool để view ajax request/response

7. PHP OOP
- Viết 1 mini crawler utility library để crawl và bóc tách data từ vnexpress, vietnamnet (sử dụng curl, ko sử dụng thư viện)
- Bao gồm 1 base class Crawler define các shared method, 2 class kế thừa implement cho VnexpressCrawler và VietnamnetCrawler
- Viết 1 test script để demo các chức năng (input URL của trang article detail cua vnexpress hoặc vietnamnet -> bóc tách thông tin title, content và date lưu vào database)


8. Javascript OOP -> Function as variable in Javascript (function expression) -> practice some function in OOP style -> $ function and Jquery (6 -> 12 hours)
- Quan sát các điểm khác nhau so với OOP trong PHP như thế nào, xem các đặc tính cơ bản của OOP sẽ được implement như thế nào trong JS
http://www.w3schools.com/js/js_function_definition.asp
http://www.w3schools.com/js/js_function_invocation.asp
http://www.w3schools.com/js/js_object_definition.asp
http://www.w3schools.com/js/js_object_methods.asp
http://www.w3schools.com/js/js_object_prototypes.asp
http://www.bolinfest.com/javascript/inheritance.php -> cần practice theo

http://osric.com/chris/accidental-developer/2008/04/the-javascript-dollar-sign-function/
https://api.jquery.com/jquery.fn.extend/
https://api.jquery.com/jQuery.extend/
http://learn.jquery.com/plugins/

9. SQL - thiết kế table cho quan hệ n-n, parent (multiple level data structure) , viết được group by, having, distint, join multiple tables, phân biệt được inner join/left join, biết sử dụng 1 số hàm math (floor,....), date time, string (concat,....) trong SQL queries 
- http://www.ntu.edu.sg/home/ehchua/programming/sql/relational_database_design.html
- http://arachnoid.com/MySQL/
- Practice tren 1 database co san nao do, dat ra cau hoi truy van thong tin va tu viet SQL (4 -> 8 hours)

10. MVC - xem va hiểu mô hình MVC (https://daveh.io/blog/the-model-view-controller-pattern) -> practice theo CodeIgniter blog tutorial (http://blog.pisyek.com/create-a-simple-blog-using-codeigniter-2-part-1/) (6 -> 12 hours)

11. Biết sử dụng các functions để debug javascript (console.log, alert, Inspect tool -> Debugger -> break point, variable watch/event), php (die, print_r, var_dump, debug_print_backtrace, debug_backtrace)
- https://scotch.io/tutorials/debugging-javascript-with-chrome-devtools-breakpoints
- Áp dụng vào debug_print_backtrace vào codeigniter để xem luồng chạy của code đi qua các file nào, function nào

12. Viết giải thuật sắp xếp, đệ qui, hiểu và luyện cấu trúc dữ liệu stack, queue
- https://www.sitepoint.com/php-data-structures-1/
- Đọc 1 multiple levels menu từ database và output ra tree HTML

13. error_reporting (setting via php code or php.ini), URL rewrite (friendly URL) with htaccess (apache) or vhosts (nginx)
- Nắm được cơ bản regular expression (ứng dụng trong form data validate, PHP preg_xxx function, rewrite URL rule)
- Tập rewrite URL detail của blog từ dạng /posts/detail/345/ thành URL friendly /detail/an-example-post-345.html

14. Tìm hiểu cách tổ chức code khoa học của 1 example application
https://github.com/onlinebizsoft/mini
- Sắp xếp folder
- Mô hình MVC
- Cách chia controller
- Tách riêng config file

15. - Work process https://www.dropbox.com/s/h0ok7suxoa4fkru/Work%20process%20guide.docx?dl=0
- Coding standard https://www.dropbox.com/s/ssythktayfwgzk3/Coding%20Standard.docx?dl=0


Tài liệu tham khảo : w3schools, PHP Manual.chm và google các article bằng tiếng Anh (ví dụ "session and cookie PHP explained")
---------------------------

Mini project

1. Implement HTML/CSS (can use the HTML Responsive done in practice to start)
- Split CSS to style.css and layout.css and responsive.css
- Custom web font
- Retina and sprite ready
http://stackoverflow.com/questions/15551287/how-to-test-a-website-for-retina-on-windows-without-an-actual-retina-display
http://www.w3schools.com/css/css_image_sprites.asp
- If the responsive use bootstrap, please make sure below rules
+ Customize bootstrap and remove unnecessary component http://getbootstrap.com/customize/
+ Don't edit bootstrap files directly, need customize in correct way https://teckstack.com/change-breakpoints-bootstrap
- Perfect pixel and test browsers

2. Design database to store content or products and categories
- n-n relation (categories/products), 1-n (product/product images),....
- Multiple level categories

3. Prepare an admin HTML interface for grid, table, login layout,....

4. Build simple MVC framework and create prototype (structure, files (controller/actions/model, view files),....)

4. Implement code for backend management
- Admin login (support "Remember me" cookie)
- Add/edit/delete content, categories
- Upload product images
- Add import/export Excel feature for product information in admin
- Apply ajax in some features like logging, search

5. Integrate code for front-end, output data
- Write recursive function (đệ qui) to output all categories/menu levels 

6. Write simple jquery to top, sticky header, lazy loader plugin and apply to the design
https://api.jquery.com/jquery.fn.extend/
https://api.jquery.com/jQuery.extend/
http://learn.jquery.com/plugins/

7. Tuning fine the website
- Improve responsive design on mobile and tablet
- Verify CSS quality and code convention
- Test typography style (default style for h1, h2,....)
- Validate W3C
- Validate SEO basic (unique h1 for each page)
- Optimize google page speed score
- Index database correctly to speed up SQL query
- Rewrite menu recursive implementation in a better way to reduce SQL queries http://stackoverflow.com/questions/544632/implementing-a-hierarchical-data-structure-in-a-database

8. Move site to hosting
- Using FileZilla
- Using git command https://www.dropbox.com/s/dyrwviahxbn2eg0/common%20linux%20command.txt?dl=0
- Pratice with bitnami virtualbox
https://bitnami.com/stack/lamp/virtual-machine
https://docs.bitnami.com/virtual-machine/infrastructure/lamp/
https://docs.bitnami.com/virtual-machine/faq/#how-to-enable-the-ssh-server

=====================================================================================

(For simple template implementation position)
C. Basic Magento
Tìm hiểu Magento (quản lí trong backend và các chức năng ngoài front-end, shopping, checkout)
+ Có bao nhiêu product type khác nhau, cách setup và mục đích của từng product type
**** Khi nào sử dụng simple products và custom options, khi nào sử dụng configurable product ? *******
+ Sử dụng thành thạo special price, tier price, product group price
+ Product attribute set/product attributes là gì?
+ Cấu hình product attribute show trên product details thế nào?
+ Enable filter/layered navigation ngoài front-end thế nào?
+ Checkout flow ngoai front-end gom steps the nao?
+ Shipping methods la gi? Hiện ở đâu ở front-end và enable/disable ở đâu trong backend ?
+ Payment methods la gi? Hiện ở đâu ở front-end và enable/disable ở đâu trong backend ?
+ Cấu hình shipping cost dung table rate method
+ ***** Test paypal sandbox and view log file (enable Debug mode) *******
+ Orders/invoices/credit memo là gì(cần hiểu được invoice và credit memo có ý nghĩa thế nào trong quá trình process order, tạo ra invoice/credit memo thế nào)
Pending Order -> nhận được tiền -> Invoiced (create invoice) -> chuyển hàng -> Shipped (create shipment) -> KH muốn trả lại hàng -> Refund KH tiền (credit memo)
+ Phân biệt order status va order state trong Magento
+ Coupon code là gì? setup coupon code và sử dụng thế nào?
+ Setup tax rate thế nào? Ví dụ mọi sản phẩm bán ở VN thường chịu thuế VAT 10% thì sẽ cần setup thế nào
+ Hiểu các setting trong configuration section (cần hiểu được tối thiểu 50% các setting mục đích sử dụng thế nào, ảnh hưởng thế nào khi thay đổi)
Các setting của Magento nằm ở menu Admin -> System -> Configuration -> toàn bộ bên left menu
+ Hiểu về configuration scope (global level/website level/store view level)
+ ***** Sử dụng directive trong CMS pages/static blocks *******
+ Nhúng 1 static block vao trong 1 CMS page the nao? (google "how to embed static block to CMS page")
+ Nhúng 1 products block vao trong 1 CMS page the nao? (google "how to show products on home page")
+ Magento multiple stores và different languages setup thế nào
+ Translate product name cho các language khác nhau thế nào
+ Merge CSS/JS in Magento
+ Edit 1 email template trong admin (ví dụ welcome new customer email) thế nào
+ Biết sử dụng mass update action trên product grid để update many products trong 1 lần
+ Biết disable/enable Magento cache (và biết Magento cache để làm gì)
+ Quản lí users roles/permission for admin users

Tài liệu tham khảo Magento1: http://merch.docs.magento.com/ce/user_guide/Resources/pdf/magento_community_edition_user_guide.pdf và google "how to magento1" bằng tiếng Anh
Tài liệu tham khảo Magento1: http://docs.magento.com/m2/ce/user_guide/Resources/pdf/Magento_Community_Edition_2.1_User_Guide.pdf và google "how to magento2" bằng tiếng Anh

D. Basic Magento template
(magento1)
- Biết sử dụng template path hint để tìm file template
- Xem về cấu trúc thư mục template/code trong Magento
- Hiểu về cách tổ chức template và cấu trúc layout trong Magento
- Thành thạo cách tùy biến layout và block qua file XML trong template của Magento (custom layout and templates)
+ Có thể move được các block giữa các vị trí trên trang
+ Hiểu rõ về các attribute của block và các thành phẩn trong XML files như type, name, alias, template
+ Có thể thêm được 1 block có sẵn nào đó vào 1 page cụ thể
+ Biết cách remove các block không dùng đến, change layout, change template 1column/2column/....
+ Biết add/remove các file CSS/JS cần thiết
+ Nắm được tên các standard/default block có trên home page
+ Phân biệt được structure block và content block (khai báo khác nhau thế nào, hoạt động khác nhau thế nào) , hiểu về reference, update, remove và action (tìm tên các method có thể sử dụng cho action ở đâu?)
+ Biết cách truyền data cho block https://techbandhu.wordpress.com/2013/04/26/magento-pass-variables-to-block/
+ 3 cách cơ bản để đưa block vào 1 page là thông qua file layout XML (1), nhúng trong cms page/static block (2), viết trong Custom XML content của category/product/cms page (3)
+ Hiểu về magic method get/set trong Magento http://joshpennington.com/an-overview-of-magentos-get-and-set-methods/
- ****** Nắm được nguyên lí kế thừa của file template và file layout (design terminology and concepts) *******
https://code.tutsplus.com/articles/an-introduction-to-magento-design-terminology-and-concepts--cms-20644
http://inchoo.net/magento/creating-a-new-magento-theme/
+ Ko bao giờ edit template trong thư mục base
+ Ko bao h copy các files template từ base mà ko cần edit đến
- ***** Nắm được nguyên tắc tùy biến layout CHỈ thông qua file layout/local.xml ******
http://magebase.com/magento-tutorials/5-useful-tricks-for-your-magento-local-xml/
Thử mục mini-cart từ sidebar lên top header,.....
- Translate cho label/text trong Magento/template files cần làm thế nào
http://www.templatemonster.com/help/magento-how-to-manage-csv-files-translations.html
- Install va su dung AOE template hint module (magento 1.x)
- Biết file chứa thông tin database của Magento, biết cách step để move site từ 1 domain này sang 1 domain khác (hoặc change folder)
- Hiểu về file theme.xml và RWD theme package (only Magento 1.9)
+ Nắm sơ lược về SASS http://sass-lang.com/guide
http://devdocs.magento.com/guides/m1x/ce19-ee114/RWD_dev-guide.html

Tài liệu tham khảo : http://info2.magento.com/rs/magentoenterprise/images/MagentoDesignGuide.pdf và google bằng tiếng Anh

(magento2)
- Hiểu cơ bản về composer và sử dụng được composer để install Magento2
- Sử dụng được các Magento command http://devdocs.magento.com/guides/v2.0/config-guide/cli/config-cli-subcommands.html
- Biết sử dụng template path hint để tìm file template
- Xem về cấu trúc code, thư mục trong Magento
- Hiểu về cách tổ chức template và cấu trúc layout trong Magento (Theme inheritance)
- Thành thạo cách tùy biến layout và block qua file XML trong template của Magento (custom layout and templates)
+ Có thể move được các block giữa các vị trí trên trang
+ Hiểu rõ về các attribute của block và các thành phẩn trong XML files như class, name, alias, template
+ Có thể thêm được 1 block có sẵn nào đó vào 1 page cụ thể
+ Biết cách remove các block không dùng đến, change layout, change template 1column/2column/....
+ Biết add/remove các file CSS/JS cần thiết
+ Nắm được tên các standard/default block/container có trên home page
+ Phân biệt được container va block, refenceContainer va referenceBlock, hiểu về update, remove và action,........
+ Biết cách truyền data cho block thông qua arguments trong XML layout và khi nhúng trong CMS pages
http://magento-quickies.alanstorm.com/post/148665374960/magento-2-layout-arguments-vs-action-methods
+ 3 cách cơ bản để đưa block vào 1 page là thông qua file layout XML (1), nhúng trong cms page/static block (2), viết trong Custom XML content của category/product/cms page (3)
https://magento.stackexchange.com/questions/123913/magento-2-send-data-or-variable-from-the-controller-to-phtml-template-file-direc
+ Hiểu về magic method get/set trong Magento http://joshpennington.com/an-overview-of-magentos-get-and-set-methods/
- Nắm được nguyên lí override layout XML đúng cách http://devdocs.magento.com/guides/v2.1/frontend-dev-guide/layouts/layout-override.html
- Translate cho label/text trong Magento/template files cần làm thế nào
- Install va su dung Easy template hint extension
https://github.com/onlinebizsoft/magento2-easy-template-path-hints
- CSS processing với LESS trong Magento2 
+ Nắm sơ lược về less http://lesscss.org/
http://inchoo.net/magento-2/css-preprocessing-in-magento-2/
http://devdocs.magento.com/guides/v2.1/frontend-dev-guide/css-topics/css-preprocess.html
- Biết file chứa thông tin database của Magento, biết cách step để move site từ 1 domain này sang 1 domain khác (hoặc change folder)

Tài liệu tham khảo : http://devdocs.magento.com/guides/v2.0/frontend-dev-guide/bk-frontend-dev-guide.html và google bằng tiếng Anh 

E. Front-end development
1. CSS framework Bootstrap, Google Material Design
2. CSS language SASS, LESS
4. JS framework AngjularJS, ReactJS	
5. Parallax effect, Onepage scroll, Full screen height Banner/Slideshow, Full page background (background size cover)
https://github.com/Prinzhorn/skrollr
https://github.com/alvarotrigo/fullPage.js
6. Perfect pixel

Tài liệu tham khảo W3C và google bằng tiếng Anh

F. Understanding design setup
1. Use static block to setup static content
2. Use config/setting to setup some variable value
3. Use telephone, email from store config
4. Use custom attribute to show some product information
5. Use attribute setting as Show on product detail,....
-------------

Mini project 1 :
1. Setup Magento layout and features according a real PSD
+ Setup code on GIT with .gitignore correctly
+ Create correct custom theme folder structure
+ Only local.xml for layout
2. Need to reset common style for whole website to match design (button style, input style, text font, text size, colors)
3. Implement header/footer responsive (setup element/features first (static block, layout XML,....)) -> then style mobile first

Mini project 2 :
1. Install this module https://github.com/onlinebizsoft/AdaptiveResize (M2 https://github.com/trive-digital/AdaptiveResizeM2)
2. And resize product image in grid in adaptive resize mode

============================================================

(For simple Magento code position)
J. Basic Magento code
(magento1)
- Request flow https://www.dropbox.com/s/sm0jm1xx2qqfvbs/magento_request_flow.jpg?dl=0
- Biết file chứa thông tin database của Magento, biết các step để move site từ 1 domain này sang 1 domain khác (hoặc change folder)
- Nắm sơ lược về Zend framework 1 (xem theo document của official Zend site và có thể viết hello world example) https://framework.zend.com/manual/1.12/en/learning.quickstart.html
- Hiểu về magic method get/set trong Magento http://www.coolryan.com/magento/2012/04/06/magic-methods-inside-magento/
- Hiểu các yếu tố cơ bản như block/template file/model/controller
- Nắm được cấu trúc 1 module Magento và biết cách tạo/đăng kí 1 module (chưa cần chức năng - chỉ cần xuất hiện trong System -> Configuration -> Advanced -> Output)
- Nắm được mục đích của mỗi loại cache và button trong Magento cache management
https://stuntcoders.com/magento-tutorials/magento-configuration/managing-magento-cache/
- Hiểu nguyên tắc và nắm cách viết SQL setup/update (database update/installation for module)
- Nắm được nguyên lí hoạt động của observer sử dụng được
https://www.pierrefay.com/en/magento-training/event-observer.html
https://www.ashsmith.io/2012/12/making-use-of-observers-in-magento/
https://www.nicksays.co.uk/magento-events-cheat-sheet-1-7/
- ****** Biết cách customize code thông qua rewrite (rewrite model, rewrite block, rewrite helper, rewrite controller) , observer (KO BAO GIỜ sửa trực tiếp vào các file có sẵn của Magento core hay các Magento modules, phải can thiệp thông qua rewrite hoặc observer)*****
- Nắm được nguyên lí hoạt động của router và biết cách áp dụng khi cần
- Hiểu về admin grid, tab, form trong backend interface development (tiêu biểu như customers grid -> edit customer with tabs -> each tab is a form) (cần practice tạo được grid/tab/edit form)
https://www.pierrefay.com/en/magento-training/backend-module-backoffice.html
https://www.pierrefay.com/en/magento-training/admin-grid-magento-tutorial.html
- Hiểu và thao tác được với model va collection
https://indiestechtips.wordpress.com/2011/07/30/how-to-save-data-in-magento/
http://excellencemagentoblog.com/blog/2011/09/22/magento-part12-series-collection-sql-operations/
- Biết các biến registry hay sử dụng trong Magento như current_category, current_product
- Có thể tìm 1 tutorial để làm thử 1 example module nào đó để test thử.
- Hiểu mô hình database EAV, hiểu cấu trúc EAV sử dụng trong các đối tượng data của Magento như products, customers
http://i.imgur.com/WTkS1fN.png
https://inviqa.com/blog/eav-data-model
http://excellencemagentoblog.com/blog/2011/09/07/magento-eav-database-structure/
- Nắm được mục đích và nguyên lí của các indexer và table flat
http://magento.stackexchange.com/questions/22157/how-indexing-works-in-magento/22166
- Biết cách enable và check error report (var/report), Magento system log (var/log), exception log (phân biệt được các log)
- Biết cách enable developer mode, error display in index.php
- Hiểu về cron job trong Linux và biết cách view cron job tasks trong Magento với tool AOE Scheduler
- Biết reindex từ console, biết run cron job từ console
- Hiểu về extension conflict (khi nào xảy ra?) và biết cách check extension conflict với tool (https://www.dropbox.com/s/wsqs4kn2y63dp13/ExtensionConflict-all-magento-version-MEC_201.zip?dl=0)
- View some insight information of Magento with https://github.com/madalinoprea/magneto-debug
- Debug trong Magento (print_r($object->getData(), print_r($product->debug()), Varien_Debug::backtrace(), echo $collection->getSelect(), echo get_class($object), .....)
http://molotovbliss.com/debugging-tips-and-tricks-with-magento-commerce/ (ko cần xem xdebug)
http://stackoverflow.com/questions/4680116/how-do-you-display-a-magento-sql-query-as-a-string

Tài liệu tham khảo : http://devdocs.magento.com/guides/m1x/magefordev/mage-for-dev-1.html và Google các article tiếng Anh (chọn tài liệu sáng sủa, đơn giản và dễ hiểu)
http://code.tutsplus.com/tutorials/magento-custom-module-development--cms-20643
http://code.tutsplus.com/tutorials/custom-block-development-in-magento--cms-23104


(magento2)
- Hiểu cơ bản về composer và sử dụng được composer để install Magento2
- Sử dụng va hieu được các Magento command http://devdocs.magento.com/guides/v2.0/config-guide/cli/config-cli-subcommands.html
- Biết file chứa thông tin database của Magento, biết các step để move site từ 1 domain này sang 1 domain khác (hoặc change folder)
- Request flow 
http://brideo.co.uk/magento2/Request-Flow-In-Magento-2/
http://www.dckap.com/blog/request-flow-in-magento-2/
- Hiểu về PHP namespace và nắm sơ lược về Zend framework 2 https://framework.zend.com/manual/2.4/en/user-guide/overview.html
- Hiểu về magic method get/set trong Magento http://www.coolryan.com/magento/2012/04/06/magic-methods-inside-magento/
- Hiểu mô hình MVVM và các yếu tố cơ bản như block/template file/model/controller
http://alanstorm.com/magento_2_mvvm_mvc
- Nắm được cấu trúc 1 module Magento và biết cách tạo/đăng kí 1 module
- Nắm được mục đích của mỗi loại cache và button trong Magento cache management
http://devdocs.magento.com/guides/v2.0/config-guide/cli/config-cli-subcommands-cache.html
- Hiểu nguyên tắc và nắm cách viết SQL setup/update (database update/installation for module)
- Nắm được nguyên lí hoạt động của observer sử dụng được
http://devdocs.magento.com/guides/v2.0/extension-dev-guide/events-and-observers.html
https://cyrillschumacher.com/magento2-list-of-all-dispatched-events/
- ****** Biết cách customize code thông qua observer (KO BAO GIỜ sửa trực tiếp vào các file có sẵn của Magento core hay các Magento modules, phải can thiệp thông qua extend/use hoặc observer)*****
- Nắm được nguyên lí hoạt động của router và sử dụng được
- Hiểu về admin grid trong backend interface development
https://www.mageplaza.com/magento-2-module-development/create-admin-grid-magento-2.html
- Hiểu mô hình database EAV, hiểu cấu trúc EAV sử dụng trong các đối tượng data của Magento như products, customers
http://i.imgur.com/WTkS1fN.png
https://inviqa.com/blog/eav-data-model
http://devdocs.magento.com/guides/v2.0/extension-dev-guide/attributes.html
- Nắm được mục đích và nguyên lí các indexer và flat tables
- Hiểu và thao tác được với model va collection
http://tech-jeff.com/blog/models-and-collections-for-magento2
- Biết các biến registry hay sử dụng trong Magento như current_category, current_product
- Biết cách enable và check error report, Magento system log, exception log
- Biết cách enable development mode de enable cac error reporting và error messages
- Có thể tìm 1 tutorial để làm thử 1 example module nào đó để test thử.
- Hiểu về cron job trong Linux và biết cách view cron job tasks trong Magento với tool visual cron job
https://github.com/onlinebizsoft/magento2-module-cron-schedule
- View some insight information of Magento with https://github.com/onlinebizsoft/Mgt_Developertoolbar
- Debug trong Magento (print_r($object->getData(), print_r($product->debug()), Magento\Framework\Debug::backtrace(), echo $collection->getSelect(), echo get_class($object), .....)
http://molotovbliss.com/debugging-tips-and-tricks-with-magento-commerce/ (ko cần xem xdebug)
http://stackoverflow.com/questions/4680116/how-do-you-display-a-magento-sql-query-as-a-string


Tài liệu tham khảo : http://devdocs.magento.com/guides/v2.0/extension-dev-guide/bk-extension-dev-guide.html và google tiếng Anh

L. Understanding about CDN and varnish

---------------------------------
Download and install as base extension 
https://bitbucket.org/onlinebizsoft/onlinebiz_obbase
https://www.dropbox.com/s/ucqzr2tskttosfg/OnlineBiz_ExampleExtension.zip?dl=0

Mini project 1 (OnlineBiz -> EmailForm) : Write 1 email form with Magento setting to change email template, receive email address. Form data need to be saved to a table in database too (form gửi email là 1 yêu cầu phổ biến trên các dự án, visitors fill form -> submit -> Magento send email notification and information to admin and send confirmation email to visitor)
(install mail server or http://papercut.codeplex.com/ for testing sending email in localhost)

Mini proejct 2 (OnlineBiz -> IBrand) : Building Shop by brand extension (các site bán hàng ngoài việc phân loại sản phẩm theo categories thì việc phân loại sản phẩm theo nhà sản xuất cũng là 1 yêu cầu phổ biến)
- Using a product attribute (manufacturer) and define a new table to extend to extra information 
- Write grid management (add/edit/delete/grid/mass delete) in admin
- Build front-end controller/action for product listing
- Write router to rewrite raw URL to friendly slug URL (brand/apple brand/dell-latitude,....)
Mini project 3 : Following example modules in https://www.dropbox.com/s/ijwya4rxi819l3o/magento-ebook-code-example-en.pdf?dl=0 and complete as installation packages
Mini project 4 : Install some popular extensions ajax cart, quick view, load on scroll, mailchimp, onepage checkout, webapp matrix shipping, download cac community modules qua http://freegento.com/ddl-magento-extension.php
Mini project 5 : Use VF Ajax extension to build ajax feature like ajax cart,.... https://github.com/onlinebizsoft/VF_EasyAjax

==================================================================

K. SEO & ecommerce
- Google webmaster
+ sitemap XML
+ 404 errors report
+ International href lang

- Google analytic
+ Conversion rate
+ Funnel checkout
+ Realtime visitors

- Seo onpage / seo off page
- Meta tags
- Cannonical URL	
- Google Shopping
- Tracking scipts
- Indexed 404 pages and redirect 301
- Tools for broken link check, seo checker 

- Email marketing service (Mailchimp,...) and integration with Magento
- Sale chanels (amazon, ebay,...) and integration with Magento

Tài liệu tham khảo : https://www.bigcommerce.com/blog/ecommerce-seo-checklist/
https://www.shopify.com/blog/62603845-ecommerce-seo-checklist-the-fundamentals-and-best-practices-to-rank-your-site

https://www.optimizesmart.com/implementing-enhanced-ecommerce-tracking-universal-analytics/
https://www.optimizesmart.com/google-tag-manager-data-layer-explained-like-never/
https://www.optimizesmart.com/enhanced-ecommerce-tracking-via-google-tag-manager/


I. Requirement Understanding
- Pick some Magento or Wordpress modules to install and test functionality
- Pick some project requirement to read and understand

=======================================================================

(For advanced front-end position)
D. Advanced JS
1. Su dung Firebug to debug javascript (đặt breakpoint, view callstack, variable value,....)
2. Handler in dynamic DOM
3. Confict function name $ in some popular library jquery and prototype, mootools
4. JS framework AngularJS (not libraries like jquery/prototype)
5. Node.JS

E. Advanced CSS
1. Mixins 
2. LESS
3. Scale element website (https://codepen.io/cRckls/pen/mcGCL)
4. CSS3
5. HTML5

Mini project :

==========================================================================

(For advanced developer position)
C. Advanced PHP
1. Namespace, Autoloading, Overloading, Magic methods 
2. Webservice, API integration
3. Read and write XML
4. Export to PDF, Excel
5. PHP on console, cron job
6. Deep knowledge about HTTP Header and caching/expiration
7. Regular Expression
8. UTF-8 http://www.phptherightway.com/#php_and_utf8
9. Design pattern http://www.phptherightway.com/pages/Design-Patterns.html
https://sourcemaking.com/design_patterns
10. Xdebug

G. Advanced web
- Htaccess/rewrite
- Command line (import/export database)
- MySQl configration
- Apache log
- Error log
- Composer
- UML design (user case, activity, component, class) -> database design -> software architect

F. Advanced SQL
1. Cach viet SQl de insert multiple records (dung trong he thong import high performance)
2. Cach viet SQL de clone 1 record tren cung 1 table
3. Copy data tu 1 field cua table nay sang 1 table khac
4. Copy data tu 1 table sang 1 table khac
5. Danh index de tang toc do where and join
6. EAV database model
7. Optimize SQL (using EXPLAIN and SQL profiler)
For example
SELECT * FROM `sales_flat_order_payment` left join `sales_flat_order` on `sales_flat_order_payment`.`parent_id`=`sales_flat_order`.`entity_id` and `customer_id`=68420
SELECT * FROM `sales_flat_order_payment` where `parent_id` in (select entity_id from `sales_flat_order` where `customer_id`=68420)

==============================================================================

(For advanced Magento developer)
- Best practice for customization/rewrite
- Biết sử dụng dispatch event
- Biết implement caching cho block, model/data,.... (hiểu cache key, biết cách kiểm tra 1 block có cache hay chưa)
- Magento translation inside http://excellencemagentoblog.com/blog/2015/05/20/magento-translate-system/
- Working with Magento API
- Working with quote total collect
- Working with price rule calculation
- Working with tax calculation
- Working with indexer, eav/flat data
- Working with report http://magentotutorial.org/how-to-create-magento-custom-reports/
- Working with Varnish
- Debugging in Magento (debug functions, all SQL log, observer information, conflict)
http://www.slideshare.net/ivanchepurnyi/magento-indexes
http://excellencemagentoblog.com/blog/category/magento-blogs/nuts-bolts
http://excellencemagentoblog.com/blog/2015/09/02/magento-url-rewrite-process/
http://excellencemagentoblog.com/blog/2015/07/14/magento-cache-system-basic-concepts/
http://excellencemagentoblog.com/blog/2011/09/07/magento-eav-database-structure/
http://excellencemagentoblog.com/blog/2015/06/10/magento-eav-concepts/
http://excellencemagentoblog.com/blog/2015/06/10/magento-flat-tables/
http://excellencemagentoblog.com/blog/2015/05/20/magento-translate-system/
http://excellencemagentoblog.com/blog/2015/05/18/magento-extension-conflicts/
http://excellencemagentoblog.com/blog/2015/05/29/magento-unique-blocks/
http://excellencemagentoblog.com/blog/2015/05/29/magento-design-and-layout-initialization/
http://molotovbliss.com/debugging-tips-and-tricks-with-magento-commerce/
http://www.blog.magepsycho.com/utilizing-debug_backtrace-function-for-magento-debugging/

(Migratite to Magento2)
- Request flow
http://brideo.co.uk/magento2/Request-Flow-In-Magento-2/
http://www.dckap.com/blog/request-flow-in-magento-2/
- Folder structure https://darshanbhavsar.wordpress.com/2015/02/03/magento-2-directory-structure/
- Database structure https://www.ubertheme.com/magento2/infographic-magento-1-vs-magento-2-database-structure/
- MVVM in Magento2 (instead of MVC) http://alanstorm.com/magento_2_mvvm_mvc
- Setup SQL http://inchoo.net/magento-2/setup-scripts-magento-2/
- Observer http://blog.belvg.com/declaring-observers-in-magento-2-0.html
- Override http://magento.stackexchange.com/questions/86497/how-to-override-core-block-model-and-controller-in-magento2
- Model/collection http://magehit.com/blog/way-to-use-model-and-collection-in-magento-2/
- Routing http://inchoo.net/magento-2/routing-in-magento-2/
- Indexer http://devdocs.magento.com/guides/v2.0/extension-dev-guide/indexing.html

I. cac tinh nang, module da lam
1. cancel invoice order when unpaid
2. coundown 
3 ledproject 
	- page tao project
	- page edit project 
	- page add san pham vao project
	- page edit spham trong project 
	- page send quote cho project
	- page view project do

4.custom lai invoice pdf, order pdf, increment pdf, shipment pdf 
5. them attribute de thay doi watermark cho mot product cu the
6. custom de add multi option, custom hien thi price tren listpage, productpage
7. addcompany column toi cac order, invoice
8. them attribute category trong backend
9. module customer trade

B. frontend
1. cac theme da trien khai
  - lightrabbit
  - lightwell
  - porto
  - fidelity
2. muon tao mot them moi can nhung j ?
	- theme.xml (xac dinh parent theme)
	- registration.php (xac dinh duoc link den theme do')
	- media/preview.jpg (duoc xac dinh trong theme.xml neu k khai bao thi k can file jpg nay)
	- composer.json

3. lam sao de hieu duoc nhung file css nao duoc chen vao website.
	- bat nguon tu module theme khai bao 2 file styles-l.css va styles-m.css
	- tu 2 file nay ta se phai tim den 2 file styles-less va styles-m.less xem no import nhung class nao vao
